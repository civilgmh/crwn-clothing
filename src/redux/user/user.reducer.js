import { UserActionTypes } from './user.types';

const INITIAL_STATE = {
  currentUser: null
};

// if state is null, that is considered a valid value and won't get initialized with INITIAL_STATE
const userReducer = (state = INITIAL_STATE, action) => {
  // don't have to use switch, could use if-else
  switch (action.type) {
    case UserActionTypes.SET_CURRENT_USER:
      return {
        ...state,
        currentUser: action.payload
      };
    default:
      return state;
  }
};

export default userReducer;