import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/auth';

const config = {
    apiKey: "AIzaSyAS_J-d5xx76kjdbU-5xOY16vYXv10SNpY",
    authDomain: "crwn-db-bf5a6.firebaseapp.com",
    databaseURL: "https://crwn-db-bf5a6.firebaseio.com",
    projectId: "crwn-db-bf5a6",
    storageBucket: "crwn-db-bf5a6.appspot.com",
    messagingSenderId: "115605333372",
    appId: "1:115605333372:web:984f2ddc5c28060580f0f9",
    measurementId: "G-C0XQ6B84W3"
};

firebase.initializeApp(config);

export const createUserProfileDocument = async (userAuth, additionalData) => {
  if (!userAuth) return;

  const userRef = firestore.doc(`users/${userAuth.uid}`);

  const snapShot = await userRef.get();

  if (!snapShot.exists) {
    const { displayName, email } = userAuth;
    const createdAt = new Date();
    try {
      await userRef.set({
        displayName,
        email,
        createdAt,
        ...additionalData
      });
    } catch (error) {
      console.log('error creating user', error.message);
    }
  }

  return userRef;
};

export const auth = firebase.auth();
export const firestore = firebase.firestore();

const provider = new firebase.auth.GoogleAuthProvider();
provider.setCustomParameters({ prompt: 'select_account' });
export const signInWithGoogle = () => auth.signInWithPopup(provider);

export default firebase;